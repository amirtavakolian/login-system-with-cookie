<?php


function runQuery($query, $runFetch = 0)
{
  global $cnt;

  $runQuery = mysqli_query($cnt, $query);

  if ($runFetch == 1) {
    if (mysqli_num_rows($runQuery) >= 1) {
      $fetchHld = fetch($runQuery);
      return $fetchHld;
    }
  } else {
    return $runQuery;
  }
}

function fetch($runQuery)
{
  $fetchHld = mysqli_fetch_assoc($runQuery);
  return $fetchHld;
}
