-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2019 at 11:37 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `cat_title` varchar(256) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_title`) VALUES
(1, 'برنامه نویسی تحت وب'),
(2, 'کسب و کار آنلاین'),
(3, 'دل نوشته'),
(4, 'درباره من');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(3) NOT NULL,
  `post_category_id` int(3) NOT NULL,
  `post_title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `post_author` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `post_date` date NOT NULL,
  `post_image` text COLLATE utf8_persian_ci NOT NULL,
  `post_content` text COLLATE utf8_persian_ci NOT NULL,
  `post_tags` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `post_comment_count` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `post_status` varchar(255) COLLATE utf8_persian_ci NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_category_id`, `post_title`, `post_author`, `post_date`, `post_image`, `post_content`, `post_tags`, `post_comment_count`, `post_status`) VALUES
(1, 0, 'برنامه نویسی چیست؟', 'امیر', '2019-06-26', 'images/what_is_programming.jpg', 'به زبان خیلی ساده،‌ برنامه نویسی عبارت است از نوشتن دستوراتی که یک سیستم -مثل کامپیوتر- آن‌ها را متوجه شده و دستورات ما را یکی پس از دیگری اجرا می کند. حال، یک زبان برنامه نویسی لغات، دستورات و قوانینی را در اختیار ما به عنوان یک برنامه نویس قرار می‌دهد که از آن طریق می‌توانیم با کامپیوترها و سیستم‌های کامپیوتری صحبت کنیم (دقیقاً به همان صورت که ما از واژگان زبان شیرین فارسی برای ارتباط با یکدیگر استفاده می‌کنیم و حرف همدیگر را می فهمیم!)\r\n\r\n', 'برنامه نویسی - بک اند - فرانت اند', '', 'draft'),
(2, 0, 'برنامه نویسی فول استک ', 'امیر', '2019-06-26', 'images/fullstack.png', 'حتما بارها و بارها فرانت اند، و بک اند را دیده و شنیده اید ولی معنی این دو کلمه را نمی دانید. به خودتان سخت نگیرید! بسیاری از طراحان وب نیز هستند که مدت هاست کار طراحی وب انجام می دهند اما به درستی تفاوت فرانت اند و بک اند و حتی معنی آنها را نیز نمی دانند. در این مقاله قصد داریم این دو کلمه را به ساده ترین شیوه ممکن برای شما توضیح دهیم.\r\n\r\nفرانت اند \"Frontend\" چیست؟\r\nزمانی که در مورد فرانت اند \"Frontend\" وب صحبت می کنیم، منظور آن بخشی است که قابل دیدن است و با آن تعامل می کنیم. فرانت اند \"Frontend\" معمولا از دو بخش تشکیل می شود: طراحی سایت و توسعه فرانت اند وب.\r\n\r\nدر گذشته اگر کسی از توسعه صحبت می کرد، عموما مربوط به بک اند \"Backend\" بود، اما در چند سال گذشته این موضوع بسیار تغییر کرده است و نیاز داریم میان کسی که سایت را در نرم افزاری مانند فوتوشاپ طراحی می کند و کسی که آنها را تبدیل به کد های HTML و CSS می کند، تفاوت بگذاریم. این موضوع زمانی که طراحان به سوی استفاده از جاوا اسکریپ و JQuery روی آوردند بسیار جدی تر شد.\r\n\r\n', 'فرانت - بک اند - برنامه نویسی', '', 'draft'),
(3, 0, 'برنامه نویسی چیست؟', 'امیر', '2019-06-26', 'images/what_is_programming.jpg', 'به زبان خیلی ساده،‌ برنامه نویسی عبارت است از نوشتن دستوراتی که یک سیستم -مثل کامپیوتر- آن‌ها را متوجه شده و دستورات ما را یکی پس از دیگری اجرا می کند. حال، یک زبان برنامه نویسی لغات، دستورات و قوانینی را در اختیار ما به عنوان یک برنامه نویس قرار می‌دهد که از آن طریق می‌توانیم با کامپیوترها و سیستم‌های کامپیوتری صحبت کنیم (دقیقاً به همان صورت که ما از واژگان زبان شیرین فارسی برای ارتباط با یکدیگر استفاده می‌کنیم و حرف همدیگر را می فهمیم!)\r\n\r\n', 'برنامه نویسی - بک اند - فرانت اند', '', 'draft'),
(4, 0, 'برنامه نویسی فول استک ', 'امیر', '2019-06-26', 'images/fullstack.png', 'حتما بارها و بارها فرانت اند، و بک اند را دیده و شنیده اید ولی معنی این دو کلمه را نمی دانید. به خودتان سخت نگیرید! بسیاری از طراحان وب نیز هستند که مدت هاست کار طراحی وب انجام می دهند اما به درستی تفاوت فرانت اند و بک اند و حتی معنی آنها را نیز نمی دانند. در این مقاله قصد داریم این دو کلمه را به ساده ترین شیوه ممکن برای شما توضیح دهیم.\r\n\r\nفرانت اند \"Frontend\" چیست؟\r\nزمانی که در مورد فرانت اند \"Frontend\" وب صحبت می کنیم، منظور آن بخشی است که قابل دیدن است و با آن تعامل می کنیم. فرانت اند \"Frontend\" معمولا از دو بخش تشکیل می شود: طراحی سایت و توسعه فرانت اند وب.\r\n\r\nدر گذشته اگر کسی از توسعه صحبت می کرد، عموما مربوط به بک اند \"Backend\" بود، اما در چند سال گذشته این موضوع بسیار تغییر کرده است و نیاز داریم میان کسی که سایت را در نرم افزاری مانند فوتوشاپ طراحی می کند و کسی که آنها را تبدیل به کد های HTML و CSS می کند، تفاوت بگذاریم. این موضوع زمانی که طراحان به سوی استفاده از جاوا اسکریپ و JQuery روی آوردند بسیار جدی تر شد.\r\n\r\n', 'فرانت - بک اند - برنامه نویسی', '', 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `user` varchar(128) COLLATE utf8_persian_ci NOT NULL,
  `pass` varchar(128) COLLATE utf8_persian_ci NOT NULL,
  `isadmin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `pass`, `isadmin`) VALUES
(1, 'admin', 'admin', 1),
(2, 'amir', 'amir', 0),
(3, 'reza', 'reza', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
