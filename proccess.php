<?php

require_once "db.php";
require_once "function.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (isset($_POST['submit']) && !empty($_POST['user']) && !empty($_POST['pass'])) {

    $user = trim($_POST['user']);
    $pass = trim($_POST['pass']);
    $rem = isset($_POST['rem']) ? 1 : 0;

    $query = "SELECT pass FROM users WHERE user = '$user'";
    $hld_query = runQuery($query, 1);

    if ($hld_query['pass'] == $pass) {
      if ($rem == 1) {

        $randNum = rand(99, 99999);
        $token = dechex($randNum * $randNum * $randNum * $randNum);
        $expire = time() + 60 * 60 * 24 * 365;

        mysqli_select_db($cnt, "cookies");
        $query = "SELECT * FROM cookie_table WHERE user = '$user'";

        $hld = runQuery($query);
        if (mysqli_num_rows($hld) >= 1) {

          $query = "UPDATE cookie_table
                    set token = '$token', salt = '$randNum'
                    WHERE user = '$user'";
        } else {

          $query = "INSERT INTO cookie_table (user,token, salt)
                    VALUES ('$user','$token', '$randNum')";
        }
        $hld_res = runQuery($query);
        setcookie("passport", $user . '+' . $token . '+' . $randNum, $expire, '/');
        header("Location:panel.php");
      } else {
        echo "Not remmember";
      }
    } else {
      echo "User or password is wrong";
    }
  } else {
    echo "User or password field are required";
  }
}
